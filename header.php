<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P2JJ6Z');</script>
<!-- End Google Tag Manager -->
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" />
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P2JJ6Z"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

	<div id="page" class="hfeed site">
		<?php get_template_part( 'menu-more' ); ?>
		
		<?php global $display; ?>
		<?php if ($display == 1) : ?>
		
            <header id="masthead" class="site-header" role="banner">
			<?php domainBanner(); ?>
			<div id="site-branding" class="site-branding">
				<h1><span class="hide">University of Wisconsin-Milwaukee</span></h1>
				<h2>
				    <span class="header-title-site"><a href="<?php echo esc_url( home_url( '/' ) ); ?>display" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php echo get_logotype(); ?></a></span>
				    <span class="header-title-page">
                        <?php
                            if ( in_category('stories') ) {
                                $title = "Stories of Impact"; echo $title;
                            } elseif ( in_category('alumni') ) {
                                $title = "Alumni"; echo $title;    
                            } elseif ( is_singular( 'uwmpeople' ) || is_archive( 'uwmpeople' ) ) {
                                $title = "People"; echo $title;
                            } elseif ( is_singular( 'tribe_events' ) ) {
                                $title = "Calendar"; echo $title;    
                            } else {
                                echo str_replace("DISPLAY: ", "", get_the_title());
                            }
                        ?>
                    </span>
				</h2>
				<p><em><a href="http://uwm.edu"><span class="hide">Powerful Ideas. Proven Results.</span></a></em></p>
			</div>
			
		</header><!-- #masthead -->
		
        <?php else : ?>
		
		    <header id="masthead" class="site-header" role="banner">
                <div class="header-background"></div>
                <?php domainBanner(); ?>
                <div id="site-branding" class="site-branding">
                    <h1><a href="http://uwm.edu"><span class="hide">University of Wisconsin-Milwaukee</span></a></h1>
                    <h2><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php echo get_logotype(); ?></a></h2>
                    <p><em><a href="http://uwm.edu"><span class="hide">Powerful Ideas. Proven Results.</span></a></em></p>
                </div>
                <div id="navbar" class="navbar">
                    <nav id="site-navigation" class="navigation main-navigation" role="navigation">
                        <div class="menu-more-toggle"><a href="#" title="UWM Menu"><i class="fa fa-bars"></i></a></div>
                        <div class="search-toggle"><a href="http://www4.uwm.edu/search" title="Search"><i class="fa fa-search"></i></a></div>
                        <div class="search-form-wrapper"><?php get_search_form(); ?></div>
                    
                        <h3 class="menu-toggle"><?php _e( 'Site Menu', 'twentythirteen' ); ?></h3>
                        <a class="screen-reader-text skip-link" href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentythirteen' ); ?>"><?php _e( 'Skip to content', 'twentythirteen' ); ?></a>
                    
                        <?php error_reporting(0); ?>
                        <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu', 'fallback_cb' => 'false', 'before' => '<div class="menu-toggle-child"><i></i></div>', 'walker' => new twentythirteen_walker_nav_menu ) ); ?>
                        <?php wp_debug_mode(); ?>
                    
                    </nav><!-- #site-navigation -->
                </div><!-- #navbar -->
            </header><!-- #masthead -->

		<?php endif; ?>

		<div id="main" class="site-main">
