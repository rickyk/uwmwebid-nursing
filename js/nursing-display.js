( function( $ ) {

     /* Redirect home page after idle timeout (8 minutes) is reached. */
    var timeout=240000;

    $(document).bind("idle.idleTimer", function(){
        window.location.replace("http://uwm.edu/nursing/display");
    });

    $.idleTimer(timeout);

    /* Remove a tags from content (no traffic to external sites). */
    $('.single .entry-content').find('a').contents().unwrap();
    $('.single-person-container').find('a').contents().unwrap();
    $('.single-tribe_events .tribe-events-event-meta').find('a').contents().unwrap();
    $('.single-tribe_events .tribe-events-single-event-description').find('a').contents().unwrap();
    $('.single-tribe_events .tribe-events-content').find('a').contents().unwrap();
    
    /* List Category Posts link fix for video play overlay */
    $( '.video_lcp_catlist .image .fa-youtube-play' ).each( function() {
        vp = '<a href="' + $(this).prevUntil('.image').attr('href') + '"><i class="fa fa-youtube-play"></i></a>';
        $(this).replaceWith(vp);
    });    
    
    /* Magnific pop-up */
    $('.youtube-link').magnificPopup({
        disableOn: 767, // loads native youtube video on mobile
        type: 'iframe',
        // other options
        gallery: {
            // options for gallery
            enabled: true
        },
    });
    
    $('#tribe-events-footer').append('<a class="button" href="/nursing/display/calendar/">View All Events</a>');
    
	  		
} )( jQuery );