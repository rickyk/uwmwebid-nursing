( function( $ ) {

    _window = $( window );
	
	/* Magnific pop-up */
    $('.youtube-link').magnificPopup({
        type: 'iframe',
        // other options
        gallery: {
            // options for gallery
            enabled: true
        },
    });
  	 	  		
} )( jQuery );