<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>

<?php global $display, $grid; ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">
	
		<?php if ( is_single() ) : ?>
		<h1 class="entry-title"><?php the_title(); ?></h1>
		<?php else : ?>
		    <?php if ($grid == 0) : ?>
            <h1 class="entry-title">
                <a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
            </h1>
		    <?php endif; ?>	
		<?php endif; // is_single() ?>
		
		<?php if ($grid == 0) : ?>
		<div class="entry-meta">
		    
		    <span class="date"><?php echo get_the_date(); ?></span>
			
			<?php $categories_list = get_the_category_list( __( ', ', 'twentythirteen' ) );
            if ( $categories_list ) {
                echo '<span class="categories-links">' . $categories_list . '</span>';
            } ?>

            <?php $tag_list = get_the_tag_list( '', __( ', ', 'twentythirteen' ) );
            if ( $tag_list ) {
                echo '<span class="tags-links">' . $tag_list . '</span>';
            } ?>
			
		</div><!-- .entry-meta -->
		<?php endif; ?>
		
		<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
		<div class="entry-thumbnail">
			<?php if ( is_single() || is_home() ) : ?>
				<div class="img-video-play">
				    <?php the_post_thumbnail(); ?>
                    <?php 
                    $vid = get_field('youtube_video_id');
                    if( !empty($vid) ): ?>
                        <a class="image youtube-link" href="https://www.youtube.com/watch?feature=player_embedded&amp;v=<?php echo $vid; ?>"><div class="video-play"><span class="text">Play Video</span></div></a>
                    <?php endif; ?>
                </div>    
				<?php 
				$fic = get_field('featured_image_caption');
				if( !empty($fic) ): ?>
					<div class="featured-img-caption"><?php echo $fic; ?></div>
				<?php endif; ?>
			<?php else : ?>
				<?php if ($grid == 1) : ?>
                    <?php if ($display == 1) : ?>
                        <a href="<?php echo get_permalink(); ?>?display=1">
                    <?php else : ?>
                        <a href="<?php echo get_permalink(); ?>">
                    <?php endif; ?>	
                    
                    <div class="img-video-play">
                    
                        <?php the_post_thumbnail('medium'); ?>
                    
                        <?php 
                        $vid = get_field('youtube_video_id');
                        if( !empty($vid) ): ?>
                            <div class="video-play"><span class="text">Play Video</span></div>
                        <?php endif; ?>
                    
                    </div>
                    
                    <?php the_title(); ?>
                    </a>
                <?php else : ?> 
                    <a href="<?php echo get_permalink(); ?>">
                    <?php the_post_thumbnail('thumbnail'); ?>
                    </a>  
				<?php endif; ?>	
				
			<?php endif; ?>	
		</div>
		<?php endif; ?>
		
		
	
	</header><!-- .entry-header -->
	

	<?php if ( is_search() || is_archive() )  : // Only display Excerpts for Search and Archives ?>
		<div class="entry-summary">
			<?php the_excerpt(); ?>
		</div><!-- .entry-summary -->
	<?php else : ?>
		<div class="entry-content">
			<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'twentythirteen' ) ); ?>
			<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
		</div><!-- .entry-content -->
	<?php endif; ?>

	<footer class="entry-meta">
		<?php if ( is_single() && get_the_author_meta( 'description' ) && is_multi_author() ) : ?>
			<?php get_template_part( 'author-bio' ); ?>
		<?php endif; ?>
	</footer><!-- .entry-meta -->
</article><!-- #post -->
