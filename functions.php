<?php

// Remove Tabby Responsive Tabs default styles
remove_action('wp_print_styles', 'cc_tabby_css', 30);


// Set global display variables.
$url = $_SERVER['REQUEST_URI'];
$display = 0;
$grid = 0;

if (strpos($url,'/display') !== false || strpos($url,'display=1') !== false) {
    $display = 1;
}

if (strpos($url,'/category/stories/') !== false || strpos($url,'/category/alumni/') !== false) {
    $grid = 1;
}

// Load scripts.
function nursing_scripts() {
    wp_enqueue_script( 'magnific-script', get_stylesheet_directory_uri() . '/js/magnific.js', array( 'jquery'), false, false);
    wp_enqueue_script( 'nursing-script', get_stylesheet_directory_uri() . '/js/nursing.js', array( 'jquery' ), '2015-06-29', true );
    global $display;
    if ($display == 1) {
        wp_enqueue_script( 'idle-timer-script', get_stylesheet_directory_uri() . '/js/idle-timer.js', array( 'jquery'), false, false);
        wp_enqueue_script( 'nursing-display-script', get_stylesheet_directory_uri() . '/js/nursing-display.js', array( 'jquery' ), '2015-06-29', true ); 
    }
}
add_action( 'wp_enqueue_scripts', 'nursing_scripts' );

// Load styles.
function nursing_styles() {
    wp_enqueue_style('magnific-styles', get_stylesheet_directory_uri() . '/css/magnific.css' );
    wp_enqueue_style('tabby-styles', get_stylesheet_directory_uri() . '/css/tabby.css' );
    wp_enqueue_style('nursing-shared-styles', get_stylesheet_directory_uri() . '/css/nursing-shared.css' );
    if ( is_child_theme() ) {
        // load parent stylesheet first if this is a child theme
	    wp_enqueue_style( 'uwmwebid-styles', trailingslashit( get_template_directory_uri() ) . 'style.css', false );
    }
    global $display;
    if ($display == 1) {
        wp_enqueue_style('google-font', 'http://fonts.googleapis.com/css?family=Libre+Baskerville' );
        wp_enqueue_style( 'nursing-display-styles', get_stylesheet_directory_uri() . '/css/nursing-display.css' );
    } else { 
        wp_enqueue_style('nursing-styles', get_stylesheet_directory_uri() . '/css/nursing.css' );
    }    
}
add_action( 'wp_enqueue_scripts', 'nursing_styles' );

// Register menu locations.
register_nav_menu( 'display', __( 'Display Menu', 'twentythirteen' ) );


// Register sidebars.
function nursing_widgets_init() {
	
	register_sidebar( array(
		'name'          => __( 'Navigation Tabs Widget Area', 'twentythirteen' ),
		'id'            => 'sidebar-navigation-tabs',
		'description'   => __( 'The Navigation Tabs widget area.', 'twentythirteen' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	
	register_sidebar( array(
		'name'          => __( 'Navigational Header Treatment Widget Area', 'twentythirteen' ),
		'id'            => 'sidebar-navigation-header',
		'description'   => __( 'The Navigation Header widget area.', 'twentythirteen' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	
}
add_action( 'widgets_init', 'nursing_widgets_init' );



// Add body classes.
function nursing_body_class( $classes ) {
    
    global $display; global $grid;
    
    if ($display == 1) {
        $classes[] = 'nursing-display';
    }
    
    if ($grid == 1) {
        $classes[] = 'nursing-grid';
    }
    
    $url = $_SERVER['REQUEST_URI'];
    
    if (strpos($url,'/academics/undergrad/') !== false) {
        $classes[] = 'cc-undergrad';
    } elseif (strpos($url,'/academics/masters/') !== false) {
        $classes[] = 'cc-masters';
    } elseif (strpos($url,'/academics/doctoral/') !== false) {
        $classes[] = 'cc-doctoral';
    } elseif (strpos($url,'/academics/certificates/') !== false) {
        $classes[] = 'cc-certificates';              
    } else {
        $classes[] = 'cc-default';
    }
    
    $afterwidgets = get_field('side_column_content_after_widgets');
    
    if ( !empty($afterwidgets) ) {
        $classes[] = 'sidebar-primary';
    }

    if ( ( is_active_sidebar( 'sidebar-navigation-tabs' ) ) && ! is_attachment() && ! is_404() )
           $classes[] = 'sidebar-navigation-tabs';
    
    return $classes;
}
add_filter( 'body_class','nursing_body_class' );

// Add current-menu-item class for single posts.
function add_current_menu_item_class( $classes, $item ) {    
    if ( is_singular( 'uwmpeople' ) && $item->title == 'People' ) {
        $classes[] = 'current-menu-item';
    } elseif ( is_post_type_archive( 'uwmpeople' ) && $item->title == 'People' ) {
        $classes[] = 'current-menu-item';
    } elseif ( in_category( 'stories' ) && $item->title == 'Stories of Impact' ) {
        $classes[] = 'current-menu-item';
    } elseif ( in_category( 'alumni' ) && $item->title == 'Alumni' ) {
        $classes[] = 'current-menu-item';
    } elseif ( is_singular( 'tribe_events' ) && $item->title == 'Calendar' ) {
        $classes[] = 'current-menu-item';    
    }
    return $classes;
}
add_filter( 'nav_menu_css_class', 'add_current_menu_item_class', 10, 2 );

// Append query string to permalinks.
function append_query_string($url) {
    return add_query_arg($_GET, $url);
}
add_filter('the_permalink', 'append_query_string');

// Custom "read more" link for excerpts
function nursing_excerpt_more( $more ) {
    global $display;
    if ($display == 1) {
        return '... <a class="read-more" href="'. get_permalink( get_the_ID() ) . '?display=1">Read More</a>';
    } else {
        return '... <a class="read-more" href="'. get_permalink( get_the_ID() ) . '">Read More</a>';
    }    
}
add_filter( 'excerpt_more', 'nursing_excerpt_more', 11 );

// Create excerpt
function create_excerpt( $post ) { 
    setup_postdata( $post );
    $excerpt = get_the_excerpt();
    wp_reset_postdata();
    $pattern = '/<a[^>]+\>Read More<\/a\>/i';
    $replacement = '<a class="more" href="' . get_permalink( $post->ID ) . '">Read more</a>';
    $excerpt = preg_replace("$pattern", $replacement, $excerpt);
    return $excerpt;    
}

// Conditionally display page template.
/*function display_template( $template ) {
    global $display;
	if ( $display == 1 && is_page() ) {
		$new_template = locate_template( array( 'page-templates/default-display.php' ) );
		if ( '' != $new_template ) {
			return $new_template ;
		}
	}
	return $template;
}
add_filter( 'template_include', 'display_template', 99 );*/

// Callback function to insert 'styleselect' into the $buttons array
function my_mce_buttons_2( $buttons ) {
	array_unshift( $buttons, 'styleselect' );
	return $buttons;
}

// Register our callback to the appropriate filter
add_filter('mce_buttons_2', 'my_mce_buttons_2');

// Callback function to filter the MCE settings
function my_mce_before_init_insert_formats( $init_array ) {  
	// Define the style_formats array
	$style_formats = array(  
		// Each array child is a format with it's own settings
		array(  
			'title' => 'Lead-in Text',  
			'block' => 'div',  
			'classes' => 'lead-in',
			'wrapper' => true,
		),
		array(  
			'title' => 'Small Text (14 pt)',  
			'block' => 'span',  
			'classes' => 'small-text-14',
			'wrapper' => true,
		),
		array(  
			'title' => 'Small Text (12 pt)',  
			'block' => 'span',  
			'classes' => 'small-text-12',
			'wrapper' => true,
		),
		array(  
			'title' => 'Panel',  
			'block' => 'div',  
			'classes' => 'panel',
			'wrapper' => true,
		),
	);  
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = json_encode( $style_formats );  
	return $init_array;  
}
 
// Attach callback to 'tiny_mce_before_init' 
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );


/**
 * List events shortcode to use with Events Calendar plugin.
 */
function list_events_shortcode( $atts, $content = null ) {
    
    $output = $event_tax = '';
    
    extract( shortcode_atts( array (
        'category' => '',
        'numberposts' => '10',
        'include_excerpt' => 0,
        'include_datebox' => 0
    ), $atts ) );
    
    if ($category) {
        $category = sanitize_text_field( $atts['category'] );
        $event_tax = array(
            array(
                'taxonomy' => 'tribe_events_cat',
                'field' => 'slug',
                'terms' => $category
            )
        );
    }
    
    $args = array(
        'post_status' => 'publish',
        'post_type' => 'tribe_events',
        'posts_per_page' => $numberposts,
        'tax_query'=> $event_tax,
    );
    
    $query = new WP_Query( $args );
    
    //var_dump($query);
    
    if ($query->have_posts()) {
        $output = '<ul class="events-list">';
        while ($query->have_posts()) : $query->the_post();
            $post_id = get_the_ID();
            $datebox_weekday = tribe_get_start_date($post_id, true, 'D');
            $datebox_date = tribe_get_start_date($post_id, true, 'm/d');
            $startdate = tribe_get_start_date($post_id, true, 'l, F j, Y');
            $enddate = tribe_get_end_date($post_id, true, 'l, F j, Y');
            $starttime = tribe_get_start_date($post_id, true, 'g:i a');
            $endtime = tribe_get_end_date($post_id, true, 'g:i a');
            $allday = tribe_event_is_all_day($post_id);
            $excerpt = tribe_events_get_the_excerpt($post_id);
            $datetime = '<div class="date">';
                if ( $startdate !== $enddate ) {
                    $datetime .= $startdate;
                    if ( $allday == 0 ) {
                        $datetime .= ' @ ' . $starttime;
                    }
                    $datetime .= ' - ' . $enddate;
                    if ( $allday == 0 ) {
                        $datetime .= ' @ ' . $endtime;
                    }
                } else {
                    $datetime .= $startdate;
                    if ( $allday == 0 ) {
                        $datetime .= ' @ ' . $starttime . ' - ' . $endtime;
                    }
                }
            $datetime .= '</div>';
            global $display;
            if ( $display == 1 ) {
                $eventlink = get_permalink() . '?display=1';
            } else {
                $eventlink = get_permalink();
            }
            $output .= '<li>';
                if ( $include_datebox !== 0 ) {
                    $output .= '<div class="datebox"><span class="datebox-weekday">' . $datebox_weekday . '</span><span class="datebox-date">' . $datebox_date . '</span></div>';
                }
            $output .= '<div class="content"><div class="title"><a href="' . $eventlink . '">' . get_the_title() . '</a></div>' . $datetime;
                if ( $include_excerpt !== 0 ) {
                    $output .= '<div class="excerpt">' . $excerpt . '</div></div>';
                }
            $output .= '</li>';
        endwhile;
        $output .= '</ul>';
    } else {
        $output .= 'There are currently no upcoming events.';
    }

    wp_reset_query();
    return $output;
    
}
add_shortcode( 'list-events', 'list_events_shortcode' );

// Pagination
function pagination_bar() {
    global $wp_query;
    $big = 999999999; // need an unlikely integer
    $translated = __( 'Page', 'mytextdomain' ); // Supply translatable string
    echo paginate_links( array(
        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'format' => '',
        'current' => max( 1, get_query_var('paged') ),
        'total' => $wp_query->max_num_pages,
            'before_page_number' => '<span class="screen-reader-text">'.$translated.' </span>'
    ) );
}


?>