<?php
/**
 * The template part for displaying flex promo content area
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

?>

<div class="flex row">
    
    <?php 
        $flex = get_field('flex_promo');
        if (!empty($flex)) {
            $col_1 = get_field('flex_col_1');
            $col_2 = get_field('flex_col_2');
            $col_3 = get_field('flex_col_3');
    
            if ( $flex == 1 ) {
                $col_width = 12;
            } elseif ( $flex == 2 ) {
                $col_width = 6;
            } elseif ( $flex == 3 ) {
                $col_width = 4;
            }
        }         
    ?>
    
    <?php if ( (!empty($flex)) && ($flex !== 0) ) : ?>
    
        <?php $i = 1; while($i <= $flex) : ?>
                
            <div class="col-md-<?php echo $col_width; ?> col-bm-40">
                <?php $column = ${'col_' . $i}; $type = $column['el_callout_type']; if ( $type == 'image' ) : ?>
                    <?php 
                    $image = $column['el_callout_image'];
                    $url = $column['el_callout_link_url'];
                    if( !empty($url) ): echo '<a href="' . $url . '">'; endif; ?>
                        <div class="post-list-photo-lg"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></div>
                    <?php if( !empty($url) ): echo '</a>'; endif; ?>
                <?php elseif ( $type == 'editor' ) : $editor = $column['el_callout_editor']; echo $editor; ?>    
                    
                <?php else : ?>
                    <?php $spotlight = $column['el_callout_spotlight_post']; if( !empty($spotlight) ): ?>
                        <?php foreach( $spotlight as $s ): ?>
                            <?php echo '<div class="post-list post-list-photo-lg"><a href="'. get_permalink( $s->ID ) . '">' . get_the_post_thumbnail( $s->ID ) . '</a>'; ?>
                            <?php echo '<div class="post-title"><a href="'. get_permalink( $s->ID ) . '">' . get_the_title( $s->ID ) . '</a></div>'; ?>
                        
                            <?php 
                                echo '<div class="lcp_excerpt">';
                                if ($s->post_excerpt) { // excerpt set, return it
                                    echo $s->post_excerpt . '<a class="more" href="' . get_permalink( $s->ID ) . '">Read more</a>';
                                } else { // excerpt not set, create it
                                    echo create_excerpt($s);
                                }
                                echo '</div></div>';
                            ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        
        <?php $i++; endwhile; ?>
        
    <?php endif; ?>
        
</div><!-- .flex -->