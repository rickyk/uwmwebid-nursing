<?php
/**
 * The template for displaying Category pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); global $display, $grid; ?>
   
    <div id="content" class="content-area">
		<div id="primary" class="site-content" role="main">
		
		<?php if ( have_posts() ) : ?>
		
		    <?php if ($display == 0) : ?>
                <header class="page-header">
                    
                    <?php
                        $category = get_queried_object();
                        $parent = get_cat_name($category->category_parent); ?>
                    
                    
                    <h1 class="page-title">
                    
                    <?php if ($parent == 'IDEAS') : ?>IDEAS: <?php endif; ?>
                    
                    <?php printf( __( '%s', 'twentythirteen' ), single_cat_title( '', false ) ); ?></h1>
                </header><!-- .page-header -->
                <?php get_sidebar( 'content-top' ); ?>
            <?php endif; ?>

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
			    <?php get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>

			<?php if ($grid == 0) : ?>
			    <?php twentythirteen_paging_nav(); ?>
			<?php else : ?>
			    <nav class="pagination">
                <?php pagination_bar(); ?>
                </nav>
			<?php endif; ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>

		</div><!-- #primary -->
		
		<?php if ($grid == 0) : ?>
		    <?php get_sidebar( 'primary' ); ?>
		    <?php get_sidebar( 'subsidiary' ); ?>
		<?php endif; ?>
		
	</div><!-- #content -->

<?php get_footer(); ?>