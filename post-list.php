<?php global $display; global $category;

if ($display == 1) {
    $display_param = '?display=1';
} else {
    $display_param = '';
}

$view_all = '/nursing/category/' . $category . '/' . $display_param ;

?>

<div class="post-list">
 
    <?php if ($category == 'stories') : ?>
        <h3>Stories of Impact</h3>
    <?php else : ?>
        <h3>Alumni</h3>
    <?php endif; ?>    
    
    <?php 
        
        $args = array(
            'category_name' => $category,
            'posts_per_page' => 4
        );

        $category_posts = new WP_Query($args);

        if ( $category_posts->have_posts() ) : ?>
            <ul>
            <?php while ( $category_posts->have_posts()) : $category_posts->the_post();
                
                $post_link = get_permalink() . $display_param ;
                $postlist_video_url = get_post_meta( get_the_ID(), 'youtubeid', true ); ?>
                
                <li>
                    <a href="<?php echo $post_link; ?>"><?php the_post_thumbnail('thumbnail'); ?><?php if( ! empty( $postlist_video_url ) ) : ?><i class="fa fa-youtube-play"></i><?php endif; ?> </a>
                    <div class="title"><a href="<?php echo $post_link; ?>"><?php the_title() ?></a></div>
                </li>
            <?php endwhile; ?>
            </ul>  
        <?php endif; ?>
    
    <a class="button" href="<?php echo $view_all; ?>">View All</a>          

</div>