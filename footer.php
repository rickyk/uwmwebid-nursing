<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>

		</div><!-- #main -->
		<footer id="colophon" class="site-footer" role="contentinfo">
			
			
		<?php global $display; ?>
				
		<?php if ($display == 1) : ?>
			
			<div id="navbar" class="navbar">
				<nav id="site-navigation" class="navigation main-navigation" role="navigation">
					
					<h3 class="menu-toggle"><?php _e( 'Site Menu', 'twentythirteen' ); ?></h3>
					<a class="screen-reader-text skip-link" href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentythirteen' ); ?>"><?php _e( 'Skip to content', 'twentythirteen' ); ?></a>
					
					<a class="floating start-here"><span class="text">Menu</span><i class="fa fa-angle-down fa-2x"></i></a>
					
					<?php error_reporting(0); ?>
					<?php wp_nav_menu( array( 'theme_location' => 'display', 'menu_class' => 'nav-menu', 'fallback_cb' => 'false', 'before' => '<div class="menu-toggle-child"><i></i></div>' ) ); ?>
					<?php wp_debug_mode(); ?>
					
				</nav><!-- #site-navigation -->
			</div><!-- #navbar -->
			
		<?php else : ?>
			
			<div class="site-info">
			
				<div class="meta">
				
					<h2><?php echo get_logotype(); ?></h2>
			
					<?php echo get_social_media(); ?>
					
					<?php if ( has_nav_menu( 'footer' ) ) : ?>
						<?php error_reporting(0); ?>
						<?php wp_nav_menu( array( 'theme_location' => 'footer', 'menu_class' => 'footer-nav', 'fallback_cb' => 'false', 'depth' => '1' ) ); ?>
						<?php wp_debug_mode(); ?>
					<?php endif; ?>
			
					<p class="footer-copyright">&copy;<?php echo date("Y"); ?> University of Wisconsin-Milwaukee</p>
				
				</div>
				
				<div class="footer-tagline"></div>
				
			</div><!-- .site-info -->
			
			<div class="backtotop"><a href="#" title="Back to top"><i class="fa fa-chevron-up"></i></a></div>
			
		<?php endif; ?>		
			
		</footer><!-- #colophon -->
	</div><!-- #page -->
		
	<?php wp_footer(); ?>
</body>
</html>