<?php
/**
 * Template Name: Static People Profile Directory
 *
 * Displays a static people profile directory listing.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

	<div id="content" class="content-area">
		<div id="primary" class="site-content people-directory-static" role="main">
		
		<header class="entry-header">
            <h1 class="entry-title"><?php the_title(); ?></h1>
        </header><!-- .entry-header -->

            <?php if( have_rows('spp_person') ): ?>


                    <?php $i = 1; while( have_rows('spp_person') ): the_row(); 

                        $image = get_sub_field('spp_person_image');
                        $name = get_sub_field('spp_person_name');
                        $title = get_sub_field('spp_person_title');
                        $email = get_sub_field('spp_person_email');
                        $phone = get_sub_field('spp_person_phone');
                        $location = get_sub_field('spp_person_location');
                        $link = get_sub_field('spp_person_link_url');

                        ?>
                        
                        <?php if( $i == 1 ): ?><div class="row"><?php endif; ?>

                        <div class="col-md-3">
                            <div class="person">

                                <?php if( $link ): ?><a href="<?php echo $link; ?>"><?php endif; ?>
                                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
                                    <strong><?php echo $name; ?></strong><br>
                                <?php if( $link ): ?></a><?php endif; ?>
                                <p><?php echo $title; ?></p>
                                <?php if( $email ): ?>
                                    <a mailto="<?php echo $email; ?>">
                                <?php endif; ?>
                                    <p><?php echo $email; ?></p>
                                <?php if( $email ): ?>
                                    </a>
                                <?php endif; ?>
                                <?php if( $phone ): ?><p><?php echo $phone; ?></p><?php endif; ?>
                                <?php if( $location ): ?><p><?php echo $location; ?></p><?php endif; ?>

                            </div>
                        </div>
                        
                        <?php if( $i % 4 == 0 ): ?></div><div class="row"><?php endif; ?>

                    <?php $i++; endwhile; ?></div>

            <?php endif; ?>			

		</div><!-- #primary -->
		
	</div><!-- #content -->

<?php get_footer(); ?>