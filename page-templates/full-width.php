<?php
/**
 * Template Name: Full-width
 *
 * Displays a full-width page.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

    <?php 
        $header_treatment = get_field('full_width_header_treatment');
        $landing_header_text = get_field('landing_page_header_text');
        $landing_header_color = get_field('landing_page_header_color');
        $header_slideshow = get_field('full_width_header_slideshow');
        $img_caption = get_field('featured_image_caption');
        $vid = get_field('youtube_video_id');
    ?>
    
	<div id="content" class="content-area">
	
	    <header class="entry-header">
            <?php get_sidebar( 'navigation-tabs' ); ?>
            <h1 class="entry-title"><?php the_title(); ?></h1>
        </header><!-- .entry-header -->
	
	    <?php if( $header_treatment != 0 ): // if header treatment is set ?>
	    
	        <div class="header-treatment">
	        
                <?php if( $header_treatment == 1 && has_post_thumbnail() && ! post_password_required() ): // if header treatment is set to image and image exists ?>
            
                    <div class="entry-thumbnail">
                        <?php the_post_thumbnail(); ?>
                        <?php if( !empty($img_caption) ): ?>
                            <div class="featured-img-caption"><?php echo $img_caption; ?></div>
                        <?php endif; ?>
                    </div>
            
                <?php elseif( $header_treatment == 2 && has_post_thumbnail() && ! post_password_required() ): // if header treatment is set to landing page and image exists ?>
        
                    <div class="colorblock-header">
                        <div class="entry-thumbnail">
                            <?php the_post_thumbnail(); ?>
                            <?php if( !empty($img_caption) ): ?>
                                <div class="featured-img-caption"><?php echo $img_caption; ?></div>
                            <?php endif; ?>
                        </div>
                        <div class="color-block" style="background-color: #<?php echo $landing_header_color; ?>">
                            <?php echo $landing_header_text; ?>
                        </div>
                    </div>
        
                <?php elseif( $header_treatment == 3 && has_post_thumbnail() && ! post_password_required() ): // if header treatment is set to navigational and image exists ?>
        
                    <div class="colorblock-header">
                        <div class="entry-thumbnail">
                            <?php the_post_thumbnail(); ?>
                            <?php if( !empty($img_caption) ): ?>
                                <div class="featured-img-caption"><?php echo $img_caption; ?></div>
                            <?php endif; ?>
                        </div>
                        <div class="color-block">
                            <?php get_sidebar( 'navigation-header' ); ?>
                        </div>
                    </div>
                    
                <?php elseif( $header_treatment == 4 ): // if header treatment is set to slideshow ?>
        
                    <?php echo do_shortcode($header_slideshow); ?>          
            
                <?php endif; ?>
        
            </div><!-- .header-treatment -->
            
	    <?php endif; ?>
	
		<div id="primary" class="site-content" role="main">

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				
					<div class="secondary-nav"><?php get_sidebar( 'content-top' ); ?></div>
				
					<?php if( $header_treatment == 0 ): // if header treatment is set to none ?>
					
                        <?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
                        <div class="entry-header">
                            <div class="entry-thumbnail">
                                <?php the_post_thumbnail(); ?>
                                
                                <?php if( !empty($vid) ): ?>
                                    <a class="image youtube-link" href="https://www.youtube.com/watch?feature=player_embedded&amp;v=<?php echo $vid; ?>"><div class="video-play"><span class="text">Play Video</span></div></a>
                                <?php endif; ?>
                                
                                <?php if( !empty($img_caption) ): ?>
                                    <div class="featured-img-caption"><?php echo $img_caption; ?></div>
                                <?php endif; ?>
                            </div>
                        </div><!-- .entry-header -->
                        <?php endif; ?>
					
					<?php endif; ?>

					<div class="entry-content">
						<?php wp_reset_query(); the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
					</div><!-- .entry-content -->

					<footer class="entry-meta">
						<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
					</footer><!-- .entry-meta -->
					
					<?php get_sidebar( 'content-bottom' ); ?>
					
				</article><!-- #post -->
				
			<?php endwhile; ?>

		</div><!-- #primary -->
		
		<?php get_sidebar( 'primary' ); ?>
		<?php get_sidebar( 'subsidiary' ); ?>
		
	</div><!-- #content -->

<?php get_footer(); ?>