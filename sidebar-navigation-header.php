<?php
/**
 * The sidebar containing the secondary widget area, displays on posts and pages.
 *
 * If no active widgets in this sidebar, it will be hidden completely.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

if ( is_active_sidebar( 'sidebar-navigation-header' ) ) : ?>
	<div id="" class="sidebar-container" role="complementary">
        <div class="widget-area widget-nav-header">
            <?php dynamic_sidebar( 'sidebar-navigation-header' ); ?>
        </div><!-- .widget-area -->
    </div>    
<?php endif; ?>