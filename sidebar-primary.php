<?php
/**
 * The sidebar containing the footer widget area.
 *
 * If no active widgets in this sidebar, it will be hidden completely.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
 
?>

<div class="combo-sidebar-container">

<?php if ( is_active_sidebar( 'sidebar-primary' ) ) : ?>	
	<div id="secondary" class="sidebar-container" role="complementary">
		<div class="widget-area primary">
			<?php dynamic_sidebar( 'sidebar-primary' ); ?>
		</div><!-- .widget-area -->
	</div><!-- #secondary -->
<?php endif; ?>

<?php 
    $afterwidgets = get_field('side_column_content_after_widgets');
    if( !empty($afterwidgets) ): ?>
        <div id="sc-after-widgets" class="sidebar-container" role="complementary">
            <aside class="side-column-content"><?php echo $afterwidgets; ?></aside>
        </div>    
<?php endif; ?>

</div>