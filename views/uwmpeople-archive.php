<?php
/*
Template Name: UWM People Profile Directory
*/

get_header();

wp_enqueue_style( 'filter-bar-css' );
wp_enqueue_script( 'filter-bar-js' );

global $display;

if ($display == 1) {
    $display_param = '?display=1';
} else {
    $display_param = '';
}

// This requires you to use the /classification/$class/$tax
//set and define any taxonomies that may be applied to the archive page
$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
//check if a taxonomy filter is present and then if it is set the values to filter by it
if(is_object($term)) { $termname = $term->name; $class = $term->slug; } else { $class = ''; }
 
$get_uwmpeople_cats = get_terms('uwmpeople_classification', array('number' => 10, 'parent' => 0)); 
list($titan, $options) =  uwmpeople_get_plugin_options(); ?>

<div id="content" class="content-area">
    <div id="primary" class="site-content" role="main">

		<div class="filter-bar">
		    <div class="row">
		    	<div class="searchwrap">
	                <input type="search" tabindex="1" class="search-field-people" placeholder="Search..." value="" name="" title="Search:">
    	            <div class="cancel-search fa fa-times-circle"></div>
		    	</div>
                <ul class="options"> 
                    <li>
                        <ul class="view">
                            <li class="view-grid view-active">Grid</li>
                            <li class="view-list">List</li>
                        </ul>
                    </li>
                    <?php if(!empty($get_uwmpeople_cats) && !is_wp_error($get_uwmpeople_cats)) { echo '<li class="filter">Filters</li>'; } ?>
                </ul><!-- .options -->               
            </div>
            
            <?php if(!empty($get_uwmpeople_cats) && !is_wp_error($get_uwmpeople_cats)) {  	
            	
            	//RDK setting a people path var (check with chris and michael about this
            	$people_path = esc_html( $titan->getOption( 'people_path', get_the_ID() ), true );
    			if ( !empty($people_path) ) {
			        $path = site_url(esc_html($titan->getOption('people_path', get_the_ID()), true)) . $display_param ;
			    } else {
			    	$path = site_url('/people/') . $display_param ;
			    }

            	$children_exist = 0;
            	
           		foreach($get_uwmpeople_cats as $classification) {

			    	$check_child_cats = get_terms('uwmpeople_classification', array('parent' => $classification->term_id));
			    	
			    	if(!empty($check_child_cats)) {
			    		$children_exist = 1;
			    	}
			    } ?>
            	
   		            <div class="filter-panel">
   		            	<?php if($children_exist) { ?>
			                <div class="row filter-parent">
			                    <ul>
			                    	<li id="tab0" class="tab"><a href="<?php echo $path; ?>">View All</a></li>
			                    	<?php 
			                    		
									    foreach($get_uwmpeople_cats as $classification) {
									    	
									    	echo '<li id="tab' . $classification->term_id . '" class="tab"><a href="' . get_term_link( $classification ) .  $display_param . '" title="' . sprintf(__('View all post filed under %s', 'my_localization_domain'), $classification->name) . '">' . $classification->name . '</a>';
									    		
									    }
									    
			                    	?>
			                    </ul>
			                </div><!-- .filter-parent -->
			     		
				     		<?php foreach($get_uwmpeople_cats as $classification) {

						    	$get_child_cats = get_terms('uwmpeople_classification', array('parent' => $classification->term_id));
						    	$items_per_column = ceil(count($get_child_cats) / 3);
						    	$child_cat_list = '';
						    	
						    	if(!empty($get_child_cats)) {
						    		
						    		$i=1;
						    		
									$child_cat_list .= '<div id="list-tab'. $classification->term_id .'" class="row filter-list"><ul>';
									
						    		foreach($get_child_cats as $child_class) {
						    								    			
						    			$child_cat_list .= '<li class="item"><a href="' . get_term_link( $child_class ) . $display_param . '" title="' . sprintf(__('View all post filed under %s', 'my_localization_domain'), $child_class->name) . '">' . $child_class->name . '</a></li>';
						    			
						    			if ($i == $items_per_column) {
						    				$child_cat_list .= '</ul><ul>';
						    				$i=1;
						    			} else {
						    				$i++;
						    			}		
						    		}
						    		
						    		$child_cat_list .= '</ul></div>';
						    	}
						    	
						    	echo $child_cat_list;
						    } 
					    } else {
					    	$items_per_column = ceil((count($get_uwmpeople_cats)+1) / 3);
					    	$cat_list = '';
					    	$i=1;
						    
							$cat_list .= '<div id="list-tab'. $classification->term_id .'" class="row filter-list"><ul><li class="item"><a href="'. $path . '">View All</a></li>';
							
							if($items_per_column == 1) { 
								$cat_list .= '</ul><ul>';
								$i=1;
							} else {
								$i++;
							}
							
					    	foreach($get_uwmpeople_cats as $classification) {
				    								    			
				    			$cat_list .= '<li class="item"><a href="' . get_term_link( $classification ) . $display_param . '" title="' . sprintf(__('View all post filed under %s', 'my_localization_domain'), $classification->name) . '">' . $classification->name . '</a></li>';
				    			
				    			if ($i == $items_per_column) {
				    				$cat_list .= '</ul><ul>';
				    				$i=1;
				    			} else {
				    				$i++;
				    			}
					    	}
					    	
					    	$cat_list .= '</ul></div>';

					    	echo $cat_list;
					    } ?>
	
		            </div><!-- .filter-panel -->  
	           <?php } ?>  
		</div><!-- .filter-bar -->

        <?php
		
        if (file_exists(plugin_dir_path(dirname(__FILE__)) . 'views/partials/directory.php')) {
            include plugin_dir_path(dirname(__FILE__)) . 'views/partials/directory.php';
            echo $the_loop;
        }
		
        ?>
        
    </div><!-- #primary -->
</div><!-- #content -->

<?php get_footer(); ?>