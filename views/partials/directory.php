<?php

wp_enqueue_style( 'uwmpeople-style' );

?>

<?php

/*
This is a template part that is used in the archive-uwmpeople template.
This was abstracted out in order to display the same HTML using the built-in
pages, or via a shortcode that we registered.

Honestly, a lot of this are not part of views and shouldn't be in here
but alas, this is how it is.
*/

/**
 * Load in the Titan Framework
 * &
 * Get our default options from the Titan Framework
 */

list($titan, $options) =  uwmpeople_get_plugin_options();

/**
 * This is used for shortcode overrides of options
 * that are set globally or whatever.
 */
if ( isset( $atts ) )
{
    if ( isset($atts['classification']) && $atts['classification'] != null )
    {
        $options['filter_cat'] = $atts['classification'];
    }
    if ( isset($atts['order_by']) && $atts['order_by'] != null )
    {
        $options['order_by'] = $atts['order_by'];
    }
    if ( isset($atts['order_dir']) && $atts['order_dir'] != null )
    {
        $options['order_dir'] = $atts['order_dir'];
    }
    if ( isset($atts['display_name']) && $atts['display_name'] != null )
    {
        $options['display_name'] = $atts['display_name'];
    }	
}

//RDK sets the "term" (classification) to be filtered by in the $options object
if(isset($term) && !isset($options['filter_cat'])) {
	if(is_object($term)) { $options['filter_cat'] = $term->slug; } else { $options['filter_cat'] = ''; }
}

// TODO: Add in the ability to sort by custom fields

/**
 * Construct our query for WordPress. Why do it all separately
 * when we can do the logic before the query? Smart.
 */
$loop = uwmpeople_get_all_query($options);

/**
 * Lets go through the loop, and build the HTML structure.
 */
$the_loop = '';

global $display;

while ( $loop->have_posts() ) : $loop->the_post();
    
    if ($display == 1) {
        $url_to_use = get_permalink() . '?display=1';
    } else {
        $url_to_use = get_permalink();
    }
    
    $the_loop .= '<div class="person">';

    // By default, let's try to get the profile photo. This may return an integer
    // or a URL. Thanks WordPress. If profile photo is empty, then we set it to 
    // default image, which ALSO might be an integer or a URL. Hopefully this covers all cases.
	$featured_image = uwmpeople_get_featured_image( get_the_ID(), $titan);
        
    $the_loop .= '<a href="' . $url_to_use . '"><img src="' . $featured_image . '" alt="" title="profile-photo" width="150">';

    $first_name = esc_html( $titan->getOption( 'first_name', get_the_ID() ), true );
    $last_name = esc_html( $titan->getOption( 'last_name', get_the_ID() ), true );
    $full_name = $last_name . ',&nbsp;' . $first_name;

    if ( $options['display_name'] == 'firstlast' )    $full_name = $first_name . ' ' . $last_name;
    else if ( $options['display_name'] == 'first' )   $full_name = $first_name;
	
	//RDK removed div with class "person-name-title" (didn't see any references to it) added span with class "person-meta"
    $the_loop .= '<span class="person-meta">';
    $the_loop .= '<span class="person-name">' . $full_name . '</span>';

    $job_title = esc_html( $titan->getOption( 'job_title', get_the_ID() ), true );
    if ( !empty($job_title) ) $the_loop .= '<span class="person-title">' . $job_title . '</span>';

	//RDK put the closing <a> tag here so whole image/people details is clickable
    $the_loop .= '</a><span class="person-hidden">';

    $department = esc_html( $titan->getOption( 'department', get_the_ID() ), true );
    //RDK added else clause that creates a span with a non-breaking space (for list view)
    //if ( !empty($department) ) $the_loop .= '<span class="uwmpeople-department">' . $department . '</span>';
    if ( !empty($department) ) { 
    	$the_loop .= '<span class="uwmpeople-department">' . $department . '</span>'; 
    } else { 
    	$the_loop .= '<span class="uwmpeople-department">&nbsp;</span>'; 
    }

    $email = esc_html( $titan->getOption( 'email', get_the_ID() ), true );
    //RDK added else clause that creates a span with a non-breaking space (for list view)
    //if ( !empty($email) ) $the_loop .= '<span class="uwmpeople-email"><a href="mailto:' . $email . '">' . $email . '</a></span>';
    
    if (!empty($email)) {
        if ($display == 1) {
            $the_loop .= '<span class="uwmpeople-email">' . $email . '</span>';
        } else {
            $the_loop .= '<span class="uwmpeople-email"><a href="mailto:' . $email . '">' . $email . '</a></span>';
        }
    }
    
    $phone = esc_html( $titan->getOption( 'phone', get_the_ID() ), true );
    //RDK added else clause that creates a span with a non-breaking space (for list view)
    //if ( !empty($phone) ) $the_loop .= '<span class="uwmpeople-phone">' . $phone . '</span>';
    if ( !empty($phone) ) { 
    	$the_loop .= '<span class="uwmpeople-phone">' . $phone . '</span>'; 
    } else { 
    	$the_loop .= '<span class="uwmpeople-phone">&nbsp;</span>'; 
    }

    $building = esc_html( $titan->getOption( 'building', get_the_ID() ), true );
    $room = esc_html( $titan->getOption( 'room', get_the_ID() ), true );
    //RDK added else clause that creates a span with a non-breaking space (for list view)
    //if ( !empty($building) ) $the_loop .= '<span class="uwmpeople-building">' . $building . ' ' . $room . '</span>';
    if ( !empty($building) ) {
    	$the_loop .= '<span class="uwmpeople-building">' . $building . ' ' . $room . '</span>';
    } else {
    	$the_loop .= '<span class="uwmpeople-building">&nbsp;</span>';
    }

    $custom_fields = preg_replace("/[\r\n]+/", "\n", $titan->getOption('custom_fields'));
    $split = explode(PHP_EOL, $custom_fields );
    if ( is_array( $split ) && !empty( $custom_fields ) )
    {
        foreach ( $split as $data )
        {
			$field = explode( '|', $data );
			$the_loop .= '<span class="uwmpeople-' . trim($field[0]) . '">';
			if ( count($field) > 1 ) // This means we defined our own in Titan
			{
				$the_loop .= wpautop($titan->getOption( $field[0], get_the_ID() ));
			}
			else // This means we reach out and find one named this
			{
				$the_loop .= wpautop(get_post_meta( get_the_ID(), trim($field[0]), true ));
			}
			$the_loop .= '</span>';
        }
    }
    // TODO: Add in additional custom fields here, with special cases for website, date, etc.
    $the_loop .= '</span></span></div>';

endwhile;

$the_loop = '<div class="person-container"><div class="wrapper">' . $the_loop . '</div></div> <!-- This is the end -->';

/** 
 * Important! If you do not call wp_reset_query() you will cause
 * other items on the page to freak out.
 */

wp_reset_query();

?>