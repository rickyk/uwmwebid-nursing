<?php

wp_enqueue_style( 'uwmpeople-style' );

global $display;

if ($display == 1) {
    $display_param = '?display=1';
} else {
    $display_param = '';
}

$view_all = get_site_url() . '/people/directory/' . $display_param ;

/*
This is a template part that is used in the single-uwmpeople template.
This was abstracted out in order to display the same HTML using the built-in
pages, or via a shortcode that we registered.

Honestly, a lot of this are not part of views and shouldn't be in here
but alas, this is how it is.
*/

/**
 * Load in the Titan Framework
 * &
 * Get our default options from the Titan Framework
 */

list($titan, $options) =  uwmpeople_get_plugin_options();

// Must use this for shortcodes to function outside of the loop.
$post_id = $post->ID;

/**
 * This is used for shortcode overrides of options
 * that are set globally or whatever.
 */
$display_name = $titan->getOption('display_name');

if ( isset( $display_name ) )
{
    if ( isset( $display_name ) && $display_name != null )
    {
        $options['display_name'] = $display_name;
    }
}

$first_name = esc_html( $titan->getOption( 'first_name', $post_id ), true );
$last_name = esc_html( $titan->getOption( 'last_name', $post_id ), true );
$full_name = $last_name . ', ' . $first_name;

if ( $options['display_name'] == 'firstlast' )    $full_name = $first_name . ' ' . $last_name;
else if ( $options['display_name'] == 'first' )   $full_name = $first_name;

if ($display == 1) {
    $the_loop = '<div class="view-all"><a href="' . $view_all . '"><i class="fa fa-users"></i> View All People</a></div>';
} else {
    $the_loop = '<div class="view-all"><i class="fa fa-users"></i><a href="' . $view_all . '">View All</a></div>';
}

if ( $is_shortcode === true )
	$the_loop .= '<h2 class="entry-title-shortcode">' . $full_name . '</h2>';
else
	$the_loop .= '<h1 class="entry-title">' . $full_name . '</h1>';

$the_loop .= '<div class="single-person-container">';
$the_loop .= '<div class="uwmpeople-details">';

// By default, let's try to get the profile photo. This may return an integer
// or a URL. Thanks WordPress. If profile photo is empty, then we set it to 
// default image, which ALSO might be an integer or a URL. Hopefully this covers all cases.
$featured_image = uwmpeople_get_featured_image($post_id, $titan);

$the_loop .= '<div class="uwmpeople-image"><img src="' . $featured_image . '" alt="'. $full_name. '" width="200" /></div>';

$the_loop .= '<div class="uwmpeople-title-dept">';

$job_title = esc_html( $titan->getOption( 'job_title', $post_id ), true );
$the_loop .= '<div class="uwmpeople-jobtitle">' . $job_title . '</div>';

$department = esc_html( $titan->getOption( 'department', $post_id ), true );
$the_loop .= '<div class="uwmpeople-department">' . $department . '</div>';

$the_loop .= '</div>'; // .uwmpeople-title-dept

$the_loop .= '<div class="uwmpeople-contact">';

$phone = esc_html( $titan->getOption( 'phone', $post_id ), true );
if ( !empty($phone) ) $the_loop .= '<div class="uwmpeople-phone">' . $phone . '</div>';

$email = esc_html( $titan->getOption( 'email', $post_id ), true );

if (!empty($email)) {
    if ($display == 1) {
        $the_loop .= '<div class="uwmpeople-email"' . $email . '">' . $email . '</div>';
    } else {
        $the_loop .= '<a class="uwmpeople-email" href="mailto:' . $email . '">' . $email . '</a>';
    }
}

$building = esc_html( $titan->getOption( 'building', $post_id ), true );
$room = esc_html( $titan->getOption( 'room', $post_id ), true );
if ( !empty($building) ) $the_loop .= $building . ' ' . $room;

$the_loop .= '</div>'; // .uwmpeople-contact

if (has_term('', 'uwmpeople_classification')) 
{
    $the_loop .= '<ul class="uwmpeople-classification">';
    $array1 = get_terms('uwmpeople_classification', array('parent' => 0));
    $array2 = get_the_terms(get_the_ID(), 'uwmpeople_classification');
    
    foreach($array1 as $classification1) {
        
        $test1 = $classification1->term_id;
        $match = 0;
        
        foreach($array2 as $classification2) {
             $test2 = $classification2->term_id;
             if ($test1 == $test2) {
                $match = 1;
                $term_link = get_term_link( $classification1 );
                // If there was an error, continue to the next term.
                if ( is_wp_error( $term_link ) ) {
                    continue;
                }
                
                if ($display == 1) {
                    $the_loop .= '<li><a href="' . esc_url( $term_link ) . '?display=1">' . $classification1->name . '</a></li>';
                } else {
                    $the_loop .= '<li><a href="' . esc_url( $term_link ) . '">' . $classification1->name . '</a></li>';
                }

                
                
                
             }
        }
    }
    
    $the_loop .= '</ul>';
}

$the_loop .= '</div>'; // .uwmpeople-details

$the_loop .= '<div class="uwmpeople-bio">';

$the_loop .= '<div class="uwmpeople-title-dept">';

$job_title = esc_html( $titan->getOption( 'job_title', $post_id ), true );
$the_loop .= '<div class="uwmpeople-jobtitle">' . $job_title . '</div>';

$department = esc_html( $titan->getOption( 'department', $post_id ), true );
$the_loop .= '<div class="uwmpeople-department">' . $department . '</div>';

$the_loop .= '</div>'; // .uwmpeople-title-dept

$the_loop .= wpautop(do_shortcode($post->post_content));

$custom_fields = preg_replace("/[\r\n]+/", "\n", $titan->getOption('custom_fields'));
$split = explode( PHP_EOL, $custom_fields );
if ( is_array( $split ) && !empty( $custom_fields ) )
{
	foreach ( $split as $data )
	{
		$field = explode( '|', $data );
		$the_loop .= '<div class="uwmpeople-' . $field[0] . '">';
		if ( count($field) > 1 ) // This means we defined our own in Titan
		{
			$the_loop .= wpautop($titan->getOption( $field[0], $post_id )) . '<br />';
		}
		else // This means we reach out and find one named this
		{
			$the_loop .= wpautop(get_post_meta( $post_id, $field[0], true )) . '<br />';
		}
		$the_loop .= '</div>';
	}
}

$the_loop .= '</div>'; // .uwmpeople-bio
$the_loop .= '</div>'; // .person-container

wp_reset_query();