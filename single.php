<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); global $display; $category = ''; ?>

	<div id="content" class="content-area">
		<div id="primary" class="site-content" role="main">

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', get_post_format() ); ?>
				
				<?php if (in_category('stories')) {
				    $category = 'stories';
				} elseif (in_category('alumni')) {
				    $category = 'alumni';
				} ?>

			<?php endwhile; ?>

		</div><!-- #primary -->
		
		<?php if ($display == 1) : ?>
		    
		    <div id="secondary" class="sidebar-container" role="complementary">
                <div class="widget-area primary">
                    <?php get_template_part('post-list'); ?>
                </div><!-- .widget-area -->
            </div><!-- #secondary -->
		    	
		<?php else : ?>
		    <?php get_sidebar( 'primary' ); ?>	
		    <?php get_sidebar( 'subsidiary' ); ?>
		<?php endif; ?>
		
    </div><!-- #content -->

<?php get_footer(); ?>